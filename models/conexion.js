const mysql = require("mysql2");
var sqlConnection = mysql.createConnection({
    host:"localhost",
    user:"root",
    database:"sistem"
});

sqlConnection.connect(function(err){
    if(err){
        console.log("Error al conectarse" + err)
    }
    else{
        console.log("Conectado con exito");
    }
});
module.exports=sqlConnection;